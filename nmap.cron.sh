#!/bin/sh

IFACE=wlan0

IP=$(ip -o -4 a | grep $IFACE | tr -s " " | cut -d " " -f 4)
nmap -Sc -sV -O $IP -oX log/nmap.log
