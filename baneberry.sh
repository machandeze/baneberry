#/bin/sh

##################################
###     PROJET BANEBERRY       ###
# Imad Benayad & Marius Chandeze #
# configuration : config.js      #
##################################

## dependances :
# libpcap0.8
# libnetfilter-queue1
sudo apt install libnetfilter-queue1 libpcap0.8

#pour eviter l'ICMP qui bypass le proxy
sudo sysctl -w net.ipv4.conf.all.send_redirects=0

#start mitm
sudo ./bettercap -script baneberry.js
